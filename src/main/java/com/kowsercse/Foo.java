package com.kowsercse;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author kowser
 *
 */
@XmlRootElement(namespace = "http://example.com/foo")
//@Link(
//        value=@Ref(
//            resource=TodoResource.class,
//            style = Style.ABSOLUTE,
//            value = "/todo-value"
//        ),
//        rel="self"
//    )
public class Foo {

	private int id;
	private String resource;
	private List<Todo> todo = new ArrayList<Todo>();

	public Foo() {
	}

	public Foo(int id, String resource, List<Todo> todo) {
		super();
		this.id = id;
		this.resource = resource;
		this.todo = todo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public List<Todo> getTodo() {
		return todo;
	}

	public void setTodo(List<Todo> todo) {
		this.todo = todo;
	}

}
