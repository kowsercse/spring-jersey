package com.kowsercse.filter;

import java.io.IOException;
import java.util.StringTokenizer;

import javax.servlet.*;
import javax.servlet.http.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.tools.codec.Base64Decoder;
import org.w3c.tools.codec.Base64FormatException;

/**
 * Supports secure authentication. The login is obtained from either the
 * browser's <b>Basic</b> authentication or from parameters.
 * 
 * @author jcalfee
 */
@Component
public class AuthenticationFilter implements Filter {

	private @Autowired Sample sample;
	
	private final String AUTH_ATTRIBUTE = this.getClass().getName() + ".authSessionToken";
	
	private FilterConfig config;

	/**
	 * Parameters can be set on a host-by-host basis. For example:
	 * param="127.0.0.1 httpsOnly" value=false will apply only to your local
	 * environment.
	 * 
	 * <ul>
	 * <li>httpsOnly - default <b>true</b>. Change this to <b>false</b> if you
	 * want to allow http authentication.
	 * </ul>
	 */
	public void init(FilterConfig config) throws ServletException {
		this.config = config;
	}

	public String getInitParameter(String param, String localAddr, String _default) {
		String initParam = config.getInitParameter(localAddr + ' ' + param);

		if (initParam == null)
			initParam = config.getInitParameter(param);

		if (initParam == null)
			initParam = _default;

		return initParam;
	}

	/**
	 * @param _request
	 *            'logout' parameter (no value needed) will end the authorized
	 *            session
	 * @param _request
	 *            'Authorization' base64 encoded username:password send by
	 *            browser's Basic authentication
	 * @param _request
	 *            'username' and 'password' parameters
	 */
	public void doFilter(ServletRequest _request, ServletResponse _response, FilterChain filter) throws IOException, ServletException {
		
		final HttpServletRequest request = (HttpServletRequest) _request;
		final HttpServletResponse response = (HttpServletResponse) _response;
		HttpSession session = request.getSession();

		
		// Make sure this is as secure as the configuration requires.
		// 1.Browsers send the Authorization header on every request.
		// 2.Make sure the session ID says encrypted
		String scheme = request.getScheme().toLowerCase();
		if (scheme.equals("http") && "true".equals(getInitParameter("httpsOnly", request .getLocalAddr(), "true"))) {
			response.setStatus(401);
			response.getWriter().println( "You must use a secure connection (https) to authenticate.");
			return;
		}

		String logout = request.getParameter("logout");
		if (logout != null) {
			session.invalidate();
			return;
		}

		String authenticated = (String) session.getAttribute(AUTH_ATTRIBUTE);
		if (authenticated != null) {
			// already authenticated
			filter.doFilter(request, response);
			return;
		}

		// try to authenticate them
		String un = null, pw = null;

		// Basic browser authentication
		String authHeader = request.getHeader("Authorization");
		if (authHeader != null) {
			String[] unPw = userPass(authHeader);
			if (unPw != null) {
				un = unPw[0];
				pw = unPw[1];
			}
		}

		// No, see if there are parameters
		if (un == null) {
			un = request.getParameter("username");
			pw = request.getParameter("password");
		}

		if (isAuth(un, pw)) {
			
			System.out.println(sample.getFixed());
			System.out.println(sample.getSomething());

			sample.setCurrentUser(un);
			// just authenticated
//			session.setAttribute(AUTH_ATTRIBUTE, "");
			filter.doFilter(request, response);
			return;
		}

		// Unauthorized - do not call doFilter
		final String BASIC_REALM = "Basic realm=\"Login Users\"";
		response.setHeader("WWW-Authenticate", BASIC_REALM);
		response.setStatus(401);
		response.getWriter().println("Authentication failure");
	}

	/**
	 * Overwrite to provide null safe authentication.
	 * 
	 * @param un
	 * @param pw
	 * @return un.equals(pw);
	 */
	protected boolean isAuth(String un, String pw) {
		return un != null && pw != null && un.equals(pw);
	}

	/**
	 * Headers sent from the browser's build-in basic authentication.
	 * 
	 * @param authorization
	 *            request.getHeader("Authorization")
	 * @return 1:user name, 2:password
	 */
	private String[] userPass(String authorization) {
		StringTokenizer st = new StringTokenizer(authorization);
		if (st.hasMoreTokens()) {
			String basic = st.nextToken();

			// We only handle HTTP Basic authentication

			if (basic.equalsIgnoreCase("Basic")) {
				String credentials = st.nextToken();
				Base64Decoder decoder = new Base64Decoder(credentials);
				String userPass = null;
				try {
					userPass = decoder.processString();
				} catch (Base64FormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				// The decoded string is in the form
				// "userID:password".

				int p = userPass.indexOf(":");
				if (p != -1) {
					String userId = userPass.substring(0, p);
					String password = userPass.substring(p + 1);
					return new String[] { userId, password };
				}
			}
		}
		return null;
	}

	public void destroy() {

	}

}


