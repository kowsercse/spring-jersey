package com.kowsercse.filter;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import com.kowsercse.bean.Section;
import com.kowsercse.resource.SectionResource;


@Component(WebApplicationContext.SCOPE_SESSION)
public class Sample {

	private String currentUser;
	
	private static int total = 0;
	
	public Sample() {
		System.out.println("constructor");
		total++;
		System.out.println("total instance: "+ total);
	}

	public String getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(String currentUser) {
		if(this.currentUser == null) {
			this.currentUser = currentUser;
		}
	}

	public String getSomething() {
		if(currentUser != null) {
			return currentUser;
		} else {
			return "no-user";
		}
	}
	
	public String getFixed() {
		return "static-string";
	}
	
	public List<Section> getSection() {
		return SectionResource.SECTIONS;
	}

}
