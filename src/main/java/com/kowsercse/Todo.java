package com.kowsercse;

import java.net.URI;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(namespace = "http://example.com/todo")
//@Link(
//        value=@Ref(
//            resource=MyResource.class,
//            style = Style.ABSOLUTE,
//            value = "/todo-value"
//        ),
//        rel="self"
//    )
public class Todo {

//	@Ref(resource = TodoResource.class, style=Style.ABSOLUTE)
	private URI uri;

	private String id;

	private String summary;

	private String description;
	
	public Todo() {
	}
	
	public Todo(String id, String summary) {
		this.id = id;
		this.summary = summary;
	}

	@XmlAttribute
	public String getAttribute() {
		return "attribute-value-" + id;
	}

	public String getDescription() {
		return description;
	}

	public String getId() {
		return id;
	}

	public String getSummary() {
		return summary;
	}

	public URI getUri() {
		return uri;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public void setUri(URI uri) {
		this.uri = uri;
	}

}
