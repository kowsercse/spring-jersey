package com.kowsercse.bean;

public class Student {

	private int id;
	private String name;
	private Section section;

	public Student() {
	}

	public Student(int id, String name, Section section) {
		super();
		this.id = id;
		this.name = name;
		this.section = section;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Section getSection() {
		return section;
	}

	public void setSection(Section section) {
		this.section = section;
	}

}
