package com.kowsercse;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Example resource class hosted at the URI path "/myresource"
 */
@Path("/myresource")
public class MyResource {

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getIt() {
		return "Hi there!";
	}

	@Path("/path")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getPath() {
		System.out.println("Hi path");
		return "Hi Path";
	}

	// This method is called if XMLis request
	@GET
	@Produces(MediaType.TEXT_XML)
	public String sayXMLHello() {
		return "<?xml version=\"1.0\"?>" + "<hello> Hello Jersey" + "</hello>";
	}

	// This method is called if HTML is request
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String sayHtmlHello() {
		return "<html><title>Hello Jersey</title><body><h1>Hello Jersey</h1></body></html> ";
	}

	@GET
	@Path("/variable/{value1}/variable/{value2}")
	public String pathVariable(
			@PathParam("value1") String value1,
			@PathParam("value2") String value2,
			@PathParam("value1") String value3) {
		System.out.println(value1);
		System.out.println(value2);
		System.out.println(value3);
		return "variable";
	}
}
