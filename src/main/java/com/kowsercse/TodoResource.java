package com.kowsercse;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

@Path("test")
public class TodoResource {
	
	@Context
	private UriInfo uriInfo;
	
	@Context
	private HttpHeaders headers;
	
	@Context
	private SecurityContext securityContext;
	
	@Context
	private Request request;
	
	@Context
	private Application application;
	
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public Todo getById() {
		System.out.println(uriInfo.getMatchedResources());
		Todo todo = new Todo("1", "summary");
		return todo;
	}
	
	@GET
	@Path("todo")
	@Produces(MediaType.APPLICATION_XML)
	public Todo getTodo() {
		System.out.println(uriInfo.getMatchedResources());
		Todo todo = new Todo("1", "summary");
		return todo;
	}
	
	@GET
	@Path("foo")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Foo getFoo() {
		ArrayList<Todo> todo = new ArrayList<Todo>();
		System.out.println(uriInfo.getMatchedResources());
		Todo todo1 = new Todo("1", "summary");
		Todo todo2 = new Todo("2", "summary");
		Todo todo3 = new Todo("3", "summary");
		todo.add(todo1);
		todo.add(todo2);
		todo.add(todo3);
		Foo foo = new Foo(1, "foo", todo);
		return foo;
	}
	
}
