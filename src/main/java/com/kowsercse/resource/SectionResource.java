package com.kowsercse.resource;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kowsercse.bean.Section;
import com.kowsercse.filter.Sample;

@Path("section")
@Component
public class SectionResource {
	
	private @Autowired Sample sample;
	
	public static ArrayList<Section> SECTIONS = new ArrayList<Section>(100);
	static {
		for(int i=0; i<10; i++) {
			Section section = new Section(i, "Section ["+i+"]", null);
			SECTIONS.add(i, section);
		}
	}
	

	@Context
	private Application application;
	@Context
	private UriInfo uriInfo;
	@Context
	private Request request;
	@Context
	private HttpHeaders headers;
	@Context
	private SecurityContext context;
	@Context
	private javax.ws.rs.ext.Providers providers;
	
	@GET
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public List<Section> getSection() {
		return SECTIONS;
	}
	
	@POST
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Consumes(MediaType.APPLICATION_XML)
	public Section addSection(Section section) {
		Section newSection = new Section();
		newSection.setId(SECTIONS.size());
		newSection.setName(section.getName());
		SECTIONS.add(newSection);
		return newSection;
	}
	
	@Path("{sectionId}")
	@GET
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Section getSection(@PathParam("sectionId") int sectionId) {
		Section section = SECTIONS.get(sectionId);
		System.out.println(section);
		return section;
	}
	
	@Path("{sectionId}")
	@PUT
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Consumes(MediaType.APPLICATION_XML)
	public Response updateSection(@PathParam("sectionId") int sectionId, Section section) throws URISyntaxException {
		Response response = Response.status(Status.NOT_FOUND).build();
		if(sectionId < SECTIONS.size()) {
			Section existingSection = SECTIONS.get(sectionId);
			existingSection.setName(section.getName());
			existingSection.setStudent(existingSection.getStudent());
			URI location = new URI("www.exmple.com");
			return Response.ok(existingSection, MediaType.APPLICATION_XML).contentLocation(location).build();
		}
		return response;
	}
	
	@Path("{sectionId}")
	@DELETE
	public Response deleteSection(@PathParam("sectionId") int sectionId) {
		Response response = Response.status(Status.NOT_FOUND).build();
		if(sectionId < SECTIONS.size() && SECTIONS.get(sectionId) != null) {
			SECTIONS.set(sectionId, null);
			return Response.ok().build();
		}
		return response;
	}
	
	@Path("test")
	@GET
	public Response getHeader() {
		System.out.println(this.application);
		System.out.println(this.context);
		System.out.println(this.headers);
		System.out.println(this.providers);
		System.out.println(this.request);
		System.out.println(this.uriInfo);
		return Response.ok().build();
	}
	
	@GET
	@Path("spring")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public List<Section> getSpring() {
		return sample.getSection();
	}
	
}
